﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileHandling.model
{
    internal class InvalidUserinput
    {
        public const string pattern = @"^[0-9]+$";
        public void checkInput(string id)
        {
            if (!Regex.IsMatch(id, pattern))
            {
                throw new ArgumentException("enter only digit ");
            }
        }
    }
}
