﻿using FileHandling.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.repository
{
    internal interface IuserRepository
    {
        bool registerUser(User user);
    }
}
