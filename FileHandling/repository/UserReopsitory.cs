﻿using FileHandling.Fileexception;
using FileHandling.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FileHandling.repository
{
    internal class UserReopsitory : IuserRepository, IFile
    {
        List<User> users;
        public UserReopsitory()
        {
            users = new List<User>();
        }
        public bool isRestiredUser(string Username, string filename)
        {
            bool isAvailabe = false;
            using(StreamReader sr=new StreamReader(filename))
            {
                string rowline;
                while((rowline = sr.ReadLine()) != null)
                {
                    string[] rowSpliteedArray = rowline.Split(",");
                    foreach(string s in rowSpliteedArray)
                    {
                        if (s == Username)
                        {
                            //throw new UserException("user exits");
                            throw new InvalidUser("user exits!!! comes from the throw in UserRepo Line:33");
                            isAvailabe = true;
                            break;
                        }
                    }
                }
            }
            return isAvailabe;
        }
        public void CheckUser(string userName, string filename)
        {
            StreamReader sr = new StreamReader(filename);
            string rowline = sr.ReadLine();
            rowline.Split(" ");
            if (rowline.Contains(userName))
            {
                throw new InvalidUser("user exits!!! comes from the throw in UserRepo Line:48");
            }
        }

        public List<string> readContentUser(string filename)
        {
            List<string> ListToDisplay = new List<string>();
            using (StreamReader sr = new StreamReader(filename)) {
                string rowline;

                while ((rowline = sr.ReadLine()) != null)
                {
                    ListToDisplay.Add(rowline);
                    
                }


            }
            return ListToDisplay;


        }
        public void showList()
        {
            foreach(User user in users)
            {
                Console.WriteLine($"id::{user.Id}\t name::{user.Name}\t city::{user.City}");
            }
        }
        public bool registerUser(User user)
        {
            users.Add(user);
            string filename = "userDatabase.txt";
            bool status=isRestiredUser(user.Name, filename);
            if (status)
            {
                return false;
            }
            else
            {
                writeToFile(user, filename);
                return true;
            }
           
            
        }

        public void writeToFile(User user, string filename)
        {
            using(StreamWriter sw=new StreamWriter(filename, true))
            {
                sw.WriteLine($"{user.Id},{user.Name},{user.City}");
            }
        }
    }
}
