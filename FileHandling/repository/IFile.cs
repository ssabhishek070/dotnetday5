﻿using FileHandling.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.repository
{
    internal interface IFile
    {
        void writeToFile(User user, string filename);

        bool isRestiredUser(string name ,string filename);

        List<string> readContentUser( string filename);
    }
}
